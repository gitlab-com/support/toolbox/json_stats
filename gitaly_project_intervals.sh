#!/bin/bash

usage() { echo "Useage: $0 [-c <COUNT>] [-m <MINUTES>] <FILE>"; exit; }

while getopts "c:m:" opt; do
  case $opt in
    c)
      COUNT=$OPTARG
      ;;
    m)
      WINDOW=$OPTARG
      ;;
    \?)
      usage
      ;;
  esac
done
shift $(($OPTIND-1))

FILE=$1

if [ -z $COUNT ]; then
  COUNT=5
fi

if [ -z $WINDOW ]; then
  WINDOW=5
fi

(( WINDOW_SECS = $WINDOW * 60 ))

# Validate that a file was passed
if [ -z "$FILE" ]
then
  echo "Usage: $0 <FILE>"
  exit
fi

# Verify that jq is available
jq --version > /dev/null
rc=$?

if [ $rc != 0 ]
then
  echo "jq not found"
  exit
fi

# Verify that pritaly is available
pritaly --version > /dev/null
rc=$?

if [ $rc != 0 ]
then
  echo "pritaly not found"
  exit
fi

top_by_chunks=`pritaly -j $FILE \
  | jq -s 'def gettime: sub("_"; "T") | explode | (.[20:25] | implode | tonumber / 100000) as $frac | .[0:19] | implode | . + "Z" | fromdate | . + $frac; \
  def starttime: .[0].log_time | gettime; \
  def add_egroup($s): ((.log_time | (gettime - $s) / '$WINDOW_SECS') | floor) as $egrp | . + {egroup: $egrp} + {etime: ($s + $egrp * '$WINDOW_SECS' | todate)}; \
  def top_projects: group_by(.grpc.request.repoPath) \
  | sort_by(-length) \
  | [ limit('$COUNT'; .[]) \
  | {batch: .[0].etime, count: length, project: .[0].grpc.request.repoPath} ];
  starttime as $st \
  | map(select(.grpc.request.repoPath != null)) \
  | map(add_egroup($st)) \
  | group_by(.egroup) \
  | .[] \
  | length as $total_calls \
  | top_projects \
  | .[] \
  | "\(.batch) \(.count) \(.project) \($total_calls)"' \
  | tr -d '"' \
  | awk 'BEGIN { print "Top '$COUNT' Projects per '$WINDOW' Minute Span" }; \
  { if ($1 != prev)
    { printf "\n%s\nTotal Calls: %s\n\n%-8s\t%s\n%-8i\t%s\n", $1, $4, "COUNT", "PROJECT", $2, $3; }
  else
    { printf "%-8i\t%s\n", $2, $3; }
  prev = $1 }; \
  END { printf "\r\n" }'`

echo "$top_by_chunks"
